{- |

Command line
============

./calp [-verbose ""] {[-in-place ""]|[-dump [-full] ""]|[-unprocess [-in-place] ""]} <file.v>

Actions:
<empty action>: process #import statements, if -in-place, overwrite original file, otherwise print result on stdout

-dump: dumps #importashead lines that should be used to implement a module, or all #import lines if -full is also specified

-unprocess: the inverse of the default action

.v formatting/syntax/sourcefile restrictions
=================================

The syntax is line oriented:

Head:
{Definition,Lemma,Corollary} chars*
<' ' chars>*
chars '.'

Body:
'Proof.'
<' ' chars>*
{'Qed.','Defined.'}

A Head containing ':=' is considered to have a body, and is stored with a an empty body.

Module names must:
- match the filenames they are contained in, and
- be globally unique (a/Mod.v and b/Mod.v won't work)

-}

module Main where

import Control.Monad (when)
import Control.Monad.Except (ExceptT, runExceptT, liftEither, throwError, withExceptT)
import Control.Monad.Morph (lift)

import Data.List (intercalate)
import Data.Map (lookup, member)

import System.Directory (findFile)
import System.Exit (exitWith, ExitCode(ExitFailure))
import System.FilePath (searchPathSeparator)
import System.IO (hPutStrLn, stderr)
import System.Posix.Files (getFileStatus, modificationTimeHiRes, setFileTimesHiRes)

import qualified Str2Ast as SA (parse)
--parse :: Monad m => String -> [String] -> ExceptT String m File
import Ast (File(..))
import Resolver (ResolvedFile(..), resolveFile)
--resolveFile :: Monad m => (String -> ExceptT String m File) -> File -> ExceptT String m ResolvedFile
import Ast2Str (render, contract)
--render :: Monad m => ([String] -> String) -> A.ResolvedFile -> ExceptT String m [String]

import StdLib
  (
  catch,
  dirsClosure,
  getArgs,
  getEnv,
  headE,
  orElse,
  readLines,
  split,
  )

import Utils (cleanup, ebyamMorf)

main :: IO ()
main = handleError $ do
    (args, posArgs) <- catch getArgs
    file <- liftEither $ headE "Missing input file." posArgs
    ls <- catch $ readLines file

    res <-      if member "dump" args      then withExceptT ((file ++ ":") ++) $ contract (member "full" args) =<< resolve file ls
           else if member "unprocess" args then withExceptT ((file ++ ":") ++) $ unlines <$> sequence (liftEither . cleanup <$> ls)
           else                                 preprocess file ls

    if member "in-place" args
      then if unlines ls /= res
        then writeButKeepMtime file res
        else when (member "verbose" args) $ catch $ putStrLn "no change"
    else
      catch $ putStr res

-- Run ExceptT and exit with 1 if there was an error.
handleError :: ExceptT String IO () -> IO ()
handleError m = do
  e <- runExceptT m
  case e of
    Left  errmsg -> hPutStrLn stderr (errmsg :: String) >> exitWith (ExitFailure 1)
    Right _      -> return ()

findFile' :: String -> ExceptT String IO FilePath
findFile' name =
  do
    path <- getPath
    res <- lift $ findFile path name
    case res of
      Nothing -> throwError $ "File not found: " ++ name
      Just p -> return p

getPath :: ExceptT String IO [FilePath]
getPath =
  do
    (args, _) <- lift getArgs
    envPath <- lift $ getEnv "COQ_SRC_PATH"
    let path = Data.Map.lookup "R" args `orElse` fmap (split searchPathSeparator) envPath `ebyamMorf` ["."]
    dirsClosure path

loader :: String -> ExceptT String IO File
loader file = do
  path <- findFile' $ file ++ ".v"
  lns <- lift $ readLines path
  SA.parse file lns

resolve :: String -> [String] -> ExceptT String IO ResolvedFile
resolve name content = do
  f <- SA.parse name content
  resolveFile loader f

preprocess :: String -> [String] -> ExceptT String IO String
preprocess name content = do
  rf <- resolve name content
  rc <- render (intercalate " (* NL *) ") rf
  return $ unlines rc

writeButKeepMtime :: String -> String -> ExceptT String IO ()
writeButKeepMtime filename content = do
  status <- catch $ getFileStatus filename
  let mtime = modificationTimeHiRes status
  catch $ writeFile filename content
  catch $ setFileTimesHiRes filename mtime mtime
