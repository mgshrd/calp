{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TupleSections #-}


module StdLib (
  catch,
  dirsClosure,
  getArgs,
  getEnv,
  headE,
  logv,
  logvm,
  lom,
  orElse,
  readLines,
  runEST,
  split,
  splitWhere,
  takeUntilCompact,
  try,
) where

import Control.Arrow (left)
import qualified Control.Exception as Ex (try, SomeException)
import Control.Monad (join)
import Control.Monad.Except (ExceptT(..), runExceptT, liftEither)
import Control.Monad.State (StateT, runStateT)
import Control.Monad.Morph (lift)

import Data.Map (Map, insertWith, fromList)
import Data.Maybe (fromMaybe)
import Data.List (isPrefixOf, findIndex)

import System.Directory (listDirectory, doesDirectoryExist)
import qualified System.Environment as E
import qualified System.IO.Unsafe as Unsafe (unsafePerformIO)

orElse :: Maybe a -> Maybe a -> Maybe a
orElse Nothing  Nothing  = Nothing
orElse (Just v) _        = Just v
orElse _        (Just v) = Just v

try :: IO a -> IO (Either Ex.SomeException a)
try = Ex.try

catch :: IO a -> ExceptT String IO a
catch a = do
  e <- lift $ try a
  liftEither $ left show e

-- |
-- >>> getEnv "ANONEIXIXSSITNEVAR"
-- Nothing
--
-- >>> getEnv "HOME"
-- Just ...
getEnv :: String -> IO (Maybe String)
getEnv name = \case { Left _ -> Nothing; Right v -> Just v } <$> try (E.getEnv name)

-- |
-- >>> E.withArgs ["0", "-a", "x", "1", "-R", "dir", "name", "2", "3"] getArgs
-- (fromList [("R",["dir"]),("a",["x"])],["0","1","2","3"])
--
-- >>> E.withArgs ["-in-place", "-R", "../Beque", "Beque", "-R", ".bin/src", "Beque.Frame", ".bin/src/intf/Domain.v"] getArgs
-- (fromList [("R",["../Beque",".bin/src"]),("in-place",[""])],[".bin/src/intf/Domain.v"])
getArgs :: IO (Map String [String], [String])
getArgs = E.getArgs >>= (\args -> return (namedArgs args, posArgs args))

-- |
-- >>> posArgs ["0", "-a", "x", "1", "-R", "dir", "name", "2", "3"]
-- ["0","1","2","3"]
--
-- >>> posArgs ["-in-place", "-R", "../Beque", "Beque", "-R", ".bin/src", "Beque.Frame", "f"]
-- ["f"]
posArgs :: [String] -> [String]
posArgs [] = []
posArgs (h:t)
  | h == "-R"     = posArgs (ltail (ltail t))
  | and [
      "-" `isPrefixOf` h,
      not (null t),
      "-" `isPrefixOf` head t
    ] = posArgs t
  | "-" `isPrefixOf` h = posArgs (ltail t)
  | otherwise     = h:posArgs t
  where ltail xs = if null xs then [] else tail xs

-- |
-- >>> namedArgs ["0", "-a", "x", "1", "-R", "dir", "name", "2", "3"]
-- fromList [("R",["dir"]),("a",["x"])]
--
-- >>> namedArgs ["-a", "-b"]
-- fromList [("a",[""]),("b",[""])]
--
-- >>> namedArgs ["-a", "-b", ""]
-- fromList [("a",[""]),("b",[""])]
--
-- >>> namedArgs ["-a", "", "-b"]
-- fromList [("a",[""]),("b",[""])]
--
{- HLINT ignore "Use &&" -}
namedArgs :: [String] -> Map String [String]
namedArgs [] = fromList []
namedArgs (h:t)
  | and [ -- Coq path spec
          h == "-R",
          length t > 2
    ] = insertWith (++) (dropWhile (== '-') h) [head t] $ namedArgs (tail (tail t))
  | and [ -- assume flag if switch follows switch
          "-" `isPrefixOf` h,
          not (null t),
          "-" `isPrefixOf` head t
    ] = insertWith (++) (dropWhile (== '-') h) [""] $ namedArgs t
  | and [ -- switch with arg (unless last)
          "-" `isPrefixOf` h
    ] = insertWith (++) (dropWhile (== '-') h) [if not (null t) then head t else ""] $ namedArgs (if not (null t) then tail t else [])
  | otherwise -- skip arg
      = namedArgs t

readLines :: FilePath -> IO [String]
readLines f = lines <$> readFile f

dirsClosure :: [FilePath] -> ExceptT String IO [FilePath]
dirsClosure xs = join <$> sequence (dirClosure <$> xs)

dirClosure :: FilePath -> ExceptT String IO [FilePath]
dirClosure d =
  do
    dirs <- subDirs d
    transDirs <- sequence (dirClosure <$> dirs)
    return (d:join transDirs)

subDirs :: FilePath -> ExceptT String IO [FilePath]
subDirs d =
  do
    entries <- catch $ listDirectory d
    let absoluteEntries = ((d ++ "/") ++) <$> entries
    entriesWithExists <- catch $ sequence $ (\e -> (e,) <$> doesDirectoryExist e) <$> absoluteEntries
    return (fst <$> filter snd entriesWithExists)

split :: Char -> String -> [String]
split c s = words $ fmap (\e -> if e == c then ' ' else c) s

headE :: String -> [a] -> Either String a
headE msg [] = Left msg
headE _   (a:_) = Right a

lom :: Show msg => msg -> a -> a
lom msg a = Unsafe.unsafePerformIO (print msg >> return a)
logv :: Show a => a -> a
logv a = lom a a
logvm :: Show msg => Show a => msg -> a -> a
logvm msg a = lom (show msg ++ ": " ++ show a) a

-- |
-- >>> splitWhere (== 'a') ""
-- ("","")
--
-- >>> splitWhere (== 'a') "xay"
-- ("x","ay")
--
-- >>> splitWhere (== 'x') "xay"
-- ("","xay")
--
-- >>> splitWhere (== 'u') "xay"
-- ("","xay")
splitWhere :: (a -> Bool) -> [a] -> ([a], [a])
splitWhere pred' xs = splitAt (fromMaybe 0 $ findIndex pred' xs) xs

pairDist :: (Either a b, c) -> Either (a, c) (b, c)
pairDist (Left e, v) = Left (e, v)
pairDist (Right v, vv) = Right (v, vv)

runEST :: Monad m => s -> ExceptT e (StateT s m) a -> ExceptT (e, s) m (a, s)
runEST s esm =
  let m = runStateT (runExceptT esm) s in
  let m' = pairDist <$> m in
  ExceptT m'

-- |
-- >>> takeUntilCompact ('s' ==) "asdf"
-- Just ('s',"a","df")
--
-- >>> takeUntilCompact ('s' ==) "assdf"
-- Just ('s',"a","sdf")
--
-- >>> takeUntilCompact ('x' ==) "asdf"
-- Nothing
--
-- >>> takeUntilCompact ('a' ==) "asdf"
-- Just ('a',"","sdf")
takeUntilCompact :: (a -> Bool) -> [a] -> Maybe (a, [a], [a])
takeUntilCompact dec l =
  let (pre, post) = splitWhere dec l in
  if null post -- l is also null
  then Nothing
  else if null pre
       then if dec (head post)
            then Just (head post, pre, tail post)
            else Nothing -- no dec conforming a in l
       else Just (head post, pre, tail post)
