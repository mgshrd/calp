{-# LANGUAGE LambdaCase #-}

module Ast (
  File(..),
  RefKind(..),
  ResolvedFile(..),
  Src(..),
  fileName,
  mergeEq,
  names,
  objs,
  parentNames,
  refName,
  refOrig,
  resolveFile,
  source,
  srcName,
  withDependencies,
) where

import Data.List (find, nub)
import Data.Maybe (catMaybes, isJust)

import Control.Monad (when)
import Control.Monad.Except (ExceptT, throwError, liftEither, withExceptT)
import Control.Arrow (left)

import qualified Coq as C

import StdLib (takeUntilCompact)

data RefKind
  = Import String{-name-} String{-orig-}
  | ImportDef String{-name-} String{-orig-}
  | ImportAsHead (Maybe String){-kind-} String{-name-} String{-body-} String{-orig-}
  | ImportDefsUpto String{-name-} String{-orig-}
  | ImportRestDefs String{-orig-}
  | ImportRest String{-orig-}
  deriving (Eq, Show)

data Src
  = Verbatim Int String
  | Coq Int C.Obj
  | Ref Int RefKind
  | Parent Int (String, String)
  deriving (Eq, Show)

srcName :: Src -> Maybe String
srcName (Coq _ o)                         = Just $ C.name o
srcName (Ref _ (Import name _))           = Just name
srcName (Ref _ (ImportDef name _))        = Just name
srcName (Ref _ (ImportAsHead _ name _ _)) = Just name
srcName (Ref _ _)                         = Nothing
srcName _                                 = Nothing

data File = File String{-name-} [Src]{-content-} deriving (Eq, Show)
data ResolvedFile = ResolvedFile File{-file-} [ResolvedFile]{-parents-} deriving (Eq, Show)

-- Merge DAGs: return the merged graph, or the conflicting nodes.
-- DAG: each element of the list points to each earlier element.
--
-- |
-- >>> mergeDAG mergeEq [1 :: Int, 2] [2, 1]
-- Left (1,2)
--
-- >>> mergeDAG mergeEq [1 :: Int, 2] [3, 4]
-- Right [1,2,3,4]
--
-- >>> mergeDAG mergeEq [1 :: Int, 2, 3] [10, 2]
-- Right [1,10,2,3]
--
-- >>> mergeDAG mergeEq [10 :: Int, 2] [1, 2, 3]
-- Right [10,1,2,3]
--
mergeDAG :: (a -> [a] -> a -> [a] -> MergeRes) -> [a] -> [a] -> Either (a, a) [a]
mergeDAG _   [] xs = Right xs
mergeDAG _   xs [] = Right xs
mergeDAG mrg (h1:t1) (h2:t2) =
  case mrg h1 t1 h2 t2 of
  Conflict   -> Left (h1, h2)
  Merge      -> (h1:) <$> mergeDAG mrg     t1     t2
  TakeFirst  -> (h1:) <$> mergeDAG mrg     t1 (h2:t2)
  TakeSecond -> (h2:) <$> mergeDAG mrg (h1:t1)    t2

data MergeRes = Conflict | Merge | TakeFirst | TakeSecond;

mergeEq :: Eq a => a -> [a] -> a -> [a] -> MergeRes;
mergeEq h1 t1 h2 t2
    | h1 == h2   = Merge
    | h1 `elem` t2 = if h2 `elem` t1 then Conflict else TakeSecond
    | otherwise  = TakeFirst

-- Nothing if key is not in dag.
withDependencies :: String -> [C.Obj] {-dag-} -> Maybe (C.Obj, [C.Obj])
withDependencies key dag =
  let dec o = C.name o == key in
  (\res -> let (o, pre, _) = res in (o, pre)) <$> takeUntilCompact dec dag

mergeFile :: Monad m => [Src] -> [(String, Int)] {-used-} -> [C.Obj] {-dag-} -> ExceptT String m [C.Obj]
mergeFile []            _  dag = return dag
mergeFile (Coq line o:t) used dag =
  -- Coq objects don't automatically pull in depenendencies, use #importdefsupto (if the preferred #import or #importashead are not feasible for some reason)
  (o:) <$> mergeFile t ((C.name o, line):used) dag
mergeFile (Ref line (Import name _):t) used dag =
  mergeDelimited line name used dag True t
mergeFile (Ref line (ImportDef name _):t) used dag =
  mergeDelimited line name used dag True t
mergeFile (Ref line (ImportAsHead _ name _ _):t) used dag =
  mergeDelimited line name used dag True t
mergeFile (Ref line (ImportDefsUpto name _):t) used dag =
  mergeDelimited line name used dag False t
mergeFile (Ref line (ImportRestDefs _):t) used dag =
  let defs = filter C.isDefinition dag in
  let uniq = nub defs in
  let new = filter (\o -> C.name o `notElem` (fst <$> used)) uniq in
  let newUsed = ((\o -> (C.name o, line)) <$> new) ++ used in
  (new ++) <$> mergeFile t newUsed dag
mergeFile (Ref line (ImportRest _):t) used dag =
  let new = filter (\o -> C.name o `notElem` (fst <$> used)) dag in
  let newUniq = fst $ foldr (\o (res, filt) -> if C.name o `elem` filt
                                               then (res, filt)
                                               else (o:res, C.name o:filt)) ([], []) new
  in
  let newUsed = ((\o -> (C.name o, line)) <$> newUniq) ++ used in
  (newUniq ++) <$> mergeFile t newUsed dag
mergeFile (_:t) used dag = mergeFile t used dag

mergeDelimited :: Monad m => Int -> String -> [(String, Int)] -> [C.Obj] -> Bool -> [Src] -> ExceptT String m [C.Obj]
mergeDelimited line name used dag addImp t = do
  when (name `elem` (fst <$> used)) $ throwError $ show line ++ ": Importing an already existing object: " ++ name ++ " (Sprung into existence at line: " ++ show (maybe 0 snd $ find (\e -> fst e == name) used) ++ ")"
  (imp, deps) <- case withDependencies name dag of
    Nothing -> throwError $ show line ++ ": Import not found: " ++ name
    Just (imp, deps) -> return (imp, deps)
  let newDeps = filter (\o -> C.name o `notElem` (fst <$> used)) deps
  let newUsed = (name, line):(((\o -> (C.name o, line)) <$> newDeps) ++ used)
  (\dag' -> newDeps ++ (if addImp then imp:dag' else dag')) <$> mergeFile t newUsed dag

reduceM :: Monad m => m a -> (a -> a -> m a) -> [a] -> m a
reduceM mempty' _        []    = mempty'
reduceM mempty' mappend' (h:t) = mappend' h =<< reduceM mempty' mappend' t

-- Ordered object names. Used as implied dependencies between names.
-- Left: conflicting names
names :: Monad m => ResolvedFile -> ExceptT String m [C.Obj]
names (ResolvedFile (File name srcs) parents) = withExceptT (\msg -> name ++ ":" ++ msg) $ do
  parentObjNames       <- sequence $ names <$> parents
  mergedParentObjNames <- liftEither $ left show (reduceM (Right []) (mergeDAG mergeObjs) (parentObjNames :: [[C.Obj]]) :: Either (C.Obj, C.Obj) [C.Obj])
  mergeFile srcs [] (mergedParentObjNames :: [C.Obj])

{- HLINT ignore "Use guards" -}
mergeObjs :: C.Obj -> [C.Obj] -> C.Obj -> [C.Obj] -> MergeRes
mergeObjs h1 t1 h2 t2 =
  if C.name h1 == C.name h2
  then if C.isDefinition h1 == C.isDefinition h2
       then Merge
       else if C.isDefinition h1
            then TakeFirst
            else TakeSecond
  else if isJust $ find (\o -> C.name o == C.name h1) t2 -- n1 depends on n2
       then if isJust $ find (\o -> C.name o == C.name h2) t1 -- n2 depends on n1
            then Conflict
            else TakeSecond
       else TakeFirst

-- $setup
-- >>> :m +Data.List
-- >>> :m +Str2Ast
-- >>> :m +StdLib
-- >>> :m +Control.Monad.Morph
-- >>> :m +Control.Monad.Except
-- >>> :{
-- let (xv, yv, zv, findPar) = (\(x, y, z) -> (x, y, z, \k -> case Data.List.lookup k [("x", xv), ("y", yv), ("z", zv)] of { Nothing -> Left "not found"; Just e -> Right e })
--                                 ) ( 
--                                     [
--                                       "Definition a := 1.",
--                                       "Parameter b : nat.",
--                                       "Conjecture c : Prop."
--                                     ],
--                                     [
--                                       "<: x",
--                                       ".",
--                                       "Definition y := 1.",
--                                       "#importashead c",
--                                       "Proof.",
--                                       "  c proof",
--                                       "Qed.",
--                                       "#importrest"
--                                     ],
--                                     [
--                                       "<: y",
--                                       ".",
--                                       "#importashead b",
--                                       "Proof.",
--                                       "Defined.",
--                                       "#importrest"
--                                     ]
--                                   )
-- :}

-- |
-- >>> runExceptT $ resolveFile (\_ -> liftEither $ Left "") =<< (Str2Ast.parse "x" xv)
-- Right (ResolvedFile (File "x" [...]) [])
--
-- >>> runExceptT $ resolveFile (\n -> Str2Ast.parse n =<< liftEither (findPar n)) =<< (Str2Ast.parse "y" yv)
-- Right (ResolvedFile (File "y" [...]) [ResolvedFile (File "x" [...]) []])
--
-- >>> runExceptT $ resolveFile (\n -> Str2Ast.parse n =<< liftEither (findPar n)) =<< (Str2Ast.parse "z" zv)
-- Right (ResolvedFile (File "z" [...]) [ResolvedFile (File "y" [...]) [...]])
resolveFile :: Monad m => (String -> ExceptT String m File) -> File -> ExceptT String m ResolvedFile
resolveFile resolver f = do
  parents <- sequence $ resolver <$> parentNames f
  resolvedParents <- sequence $ resolveFile resolver <$> parents
  return $ ResolvedFile f resolvedParents

source :: File -> [Src]
source (File _ src) = src

fileName :: File -> String
fileName (File name _) = name

objs :: File -> [Src]
objs (File _ src) = filter (\case { Coq _ _ -> True; Ref _ ImportAsHead{} -> True; _ -> False }) src

-- |
--
-- >>> parentNames (File "" [Parent 0 ("x", "")])
-- ["x"]
--
-- >>> parentNames (File "" [Verbatim 0 "", Parent 0 ("y", "")])
-- ["y"]
parentNames :: File -> [String]
parentNames (File _ srcs) = catMaybes $ (\case { Parent _ (name, _) -> Just name; _ -> Nothing }) <$> srcs

refOrig :: RefKind -> String
refOrig (Import _ orig) = orig
refOrig (ImportDef _ orig) = orig
refOrig (ImportAsHead _ _ _ orig) = orig
refOrig (ImportDefsUpto _ orig) = orig
refOrig (ImportRestDefs orig) = orig
refOrig (ImportRest orig) = orig

refName :: RefKind -> Maybe String
refName (Import name _) = Just name
refName (ImportDef name _) = Just name
refName (ImportAsHead _ name _ _) = Just name
refName (ImportDefsUpto name _) = Just name
refName _ = Nothing

