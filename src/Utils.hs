module Utils(
  MergeRes(..),
  addFile,
  addLine,
  cleanup,
  dropEnd,
  ebyamMorf,
  mergeDAG,
  mergeEq,
  reduceM,
) where

import Control.Monad.Except (ExceptT, withExceptT)

import Data.Maybe (fromMaybe)
import Data.List (isPrefixOf)

import StdLib (splitWhere)

addLine :: Monad m => Int -> ExceptT String m a -> ExceptT String m a
addLine line = withExceptT ((show line ++ ":error: ") ++)

addFile :: Monad m => String -> ExceptT String m a -> ExceptT String m a
addFile name = withExceptT ((name ++ ":") ++)

-- Merge DAGs: return the merged graph, or the conflicting nodes.
-- DAG: each element of the list points to each earlier element.
--
-- |
-- >>> mergeDAG mergeEq [1 :: Int, 2] [2, 1]
-- Left (1,2)
--
-- >>> mergeDAG mergeEq [1 :: Int, 2] [3, 4]
-- Right [1,2,3,4]
--
-- >>> mergeDAG mergeEq [1 :: Int, 2, 3] [10, 2]
-- Right [1,10,2,3]
--
-- >>> mergeDAG mergeEq [10 :: Int, 2] [1, 2, 3]
-- Right [10,1,2,3]
--
mergeDAG :: (a -> [a] -> a -> [a] -> MergeRes) -> [a] -> [a] -> Either (a, a) [a]
mergeDAG _   [] xs = Right xs
mergeDAG _   xs [] = Right xs
mergeDAG mrg (h1:t1) (h2:t2) =
  case mrg h1 t1 h2 t2 of
  Conflict   -> Left (h1, h2)
  Merge      -> (h1:) <$> mergeDAG mrg     t1     t2
  TakeFirst  -> (h1:) <$> mergeDAG mrg     t1 (h2:t2)
  TakeSecond -> (h2:) <$> mergeDAG mrg (h1:t1)    t2

data MergeRes = Conflict | Merge | TakeFirst | TakeSecond;

mergeEq :: Eq a => a -> [a] -> a -> [a] -> MergeRes;
mergeEq h1 t1 h2 t2
    | h1 == h2   = Merge
    | h1 `elem` t2 = if h2 `elem` t1 then Conflict else TakeSecond
    | otherwise  = TakeFirst

reduceM :: Monad{-Eff-} m => m a -> (a -> a -> m a) -> [a] -> m a
reduceM mempty' _        []    = mempty'
reduceM mempty' mappend' (h:t) = mappend' h =<< reduceM mempty' mappend' t


-- |
-- >>> dropEnd 1 "asdf"
-- "asd"
--
-- >>> dropEnd 10 "asdf"
-- ""
--
-- >>> dropEnd 0 "asdf"
-- "asdf"
dropEnd :: Int -> [a] -> [a]
dropEnd n xs = fst $ splitAt (length xs - n) xs

ebyamMorf :: Maybe a -> a -> a
ebyamMorf = flip fromMaybe

-- |
-- >>> cleanup "asdf"
-- Right "asdf"
--
-- >>> cleanup "(* resolved #importashead kind name *) asdf"
-- Right "#importashead kind name"
--
-- >>> cleanup "(* resolved #import a *) oob (* asdf *) obo"
-- Right "#import a"
cleanup :: String -> Either String String
cleanup l
  | "(* resolved #import" `isPrefixOf` l =
    let (orig, _) = splitWhere (== '*') $ drop (length "(* resolved ") l in
    if null orig             then Left $ "Cannot restore from line '" ++ l ++ "'"
    else if last orig /= ' ' then Left $ "Expected space in import line before comment closing asterisk in line '" ++ l ++ "'"
    else                          Right $ init orig
  | otherwise                            = Right l
