{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TupleSections #-}

module Ast2Str (
  render,
  contract,
) where

import Control.Monad (when)
import Control.Monad.Except (ExceptT, throwError)

import Data.List (intercalate, filter, notElem, partition)
import Data.Maybe (isJust, fromMaybe)

import qualified Coq as C
import qualified Ast as A
import qualified Resolver as R

import StdLib (takeUntilCompact)

import Utils (dropEnd, addLine)

-- $setup
-- >>> :m +Data.List
-- >>> :m +Str2Ast
-- >>> :m +StdLib
-- >>> :m +Control.Monad.Morph
-- >>> :m +Control.Monad.Except
-- >>> let loader file ff = do { path' <- lift $ (ff :: String -> IO (Either String FilePath)) $ file ++ ".v"; path <- liftEither path'; lns <- lift $ readLines path; parse file lns }
-- >>> let pss name content ff = runExceptT $ do { f <- parse name content; rf <- Resolver.resolveFile ff f; return rf }
-- >>> let preprocess name content ff = runExceptT $ do { f <- parse name content; rf <- Resolver.resolveFile (\fx -> loader fx ff) f; rc <- render (intercalate " (* NL\n *) ") rf; return $ unlines rc }
-- >>> :{
-- let (xv, yv, zv, findPar) = (\(x, y, z) -> (x, y, z, \k -> case Data.List.lookup k [("x", xv), ("y", yv), ("z", zv)] of { Nothing -> Left "not found"; Just e -> Right e })
--                                 ) ( 
--                                     [
--                                       "module x"
--                                     , ""
--                                     , "Definition a := 1."
--                                     , ""
--                                     , "Parameter b : nat."
--                                     , ""
--                                     , "Conjecture c : Prop."
--                                     , ""
--                                     , "Definition aa := 2."
--                                     ],
--                                     [
--                                       "module y"
--                                     , "<: x"
--                                     , "."
--                                     , "Definition y := 3."
--                                     , ""
--                                     , ""
--                                     , "#import b"
--                                     , "#importashead c"
--                                     , "Proof."
--                                     , "  c proof"
--                                     , "Qed."
--                                     , "Conjecture conjjj : 4 = 5."
--                                     , "#importrest"
--                                     ],
--                                     [
--                                       "module z"
--                                     , "<: y"
--                                     , "."
--                                     , "#importashead b"
--                                     , "Proof."
--                                     , "Defined."
--                                     , "#importashead Axiom conjjj"
--                                     , "Parameter zp : nat."
--                                     , "#importrest"
--                                     ]
--                                   )
-- :}

-- >>> psx n = parse n =<< (liftEither $ findPar n)
-- >>> pe m = (\e -> case e of { Left v -> putStrLn $ "Left " ++ show v; Right v -> putStr $ unlines v}) =<< (runExceptT m)
-- >>> pe (render (\_ -> undefined) =<< (ExceptT $ pss "x" xv psx))
-- module x
-- <BLANKLINE>
-- Definition a := 1.
-- <BLANKLINE>
-- Parameter b : nat.
-- <BLANKLINE>
-- Conjecture c : Prop.
-- <BLANKLINE>
-- Definition aa := 2.
-- 
-- >>> pe (render (intercalate "NL\n") =<< (ExceptT $ pss "y" yv psx))
-- module y
-- <: x
-- .
-- Definition y := 3.
-- (* resolved #importashead c *)NL
-- Definition a := 1 (*...*).NL
-- Parameter b : nat (*...*).NL
-- Lemma c : Prop (*...*).
-- Proof.
--   c proof
-- Qed.
-- Conjecture conjjj : 4 = 5.
-- (* resolved #importrest *)NL
-- Definition aa := 2 (*...*).
--
-- >>> pe (render (intercalate "NL\n") =<< (ExceptT $ pss "z" zv psx))
-- module z
-- <: y
-- .
-- (* resolved #importashead b *)NL
-- Definition y := 3 (*...*).NL
-- Definition a := 1 (*...*).NL
-- Definition b : nat (*...*).
-- Proof.
-- Defined.
-- (* resolved #importashead Axiom conjjj *)NL
-- Lemma c : Prop (*...*).NL
-- Proof.NL
--   c proofNL
-- Qed.NL
-- Axiom conjjj : 4 = 5 (*...*).
-- Parameter zp : nat.
-- (* resolved #importrest *)NL
-- Definition aa := 2 (*...*).
-- |
render :: Monad m => ([String] -> String) -> R.ResolvedFile -> ExceptT String m [String]
render importRenderer rf = do
  db  <- R.resolvedObjs rf
  res <- renderSrcs [] ((\case { R.ResolvedFile (A.File _ srcs) _ -> srcs }) rf) db
  return $ (\case { Straight xs -> intercalate "\n" xs; Imported xs -> importRenderer xs }) <$> res

-- Generate skeleton file for extending a module.
-- |
-- >>> psx n = parse n =<< (liftEither $ findPar n)
-- >>> pe m = (\e -> case e of { Left v -> putStrLn $ "Left " ++ show v; Right v -> putStr v}) =<< (runExceptT m)
-- >>> pe (contract False =<< (ExceptT $ pss "x" xv psx))
-- module x
-- <BLANKLINE>
-- <BLANKLINE>
-- #importashead Definition b
-- <BLANKLINE>
-- #importashead Lemma c
-- <BLANKLINE>
--
-- >>> pe (contract True =<< (ExceptT $ pss "x" xv psx))
-- module x
-- <BLANKLINE>
-- #import a
-- <BLANKLINE>
-- #import b
-- (* #importashead Definition b *)
-- <BLANKLINE>
-- #import c
-- (* #importashead Lemma c *)
-- <BLANKLINE>
-- #import aa
--
-- >>> pe (contract False =<< (ExceptT $ pss "y" yv psx))
-- module y
-- <: x
-- .
-- <BLANKLINE>
-- <BLANKLINE>
-- #importashead Definition b
-- #importashead Lemma conjjj
-- #importrest
--
-- >>> pe (contract True =<< (ExceptT $ pss "y" yv psx))
-- module y
-- <: x
-- .
-- #import y
-- <BLANKLINE>
-- <BLANKLINE>
-- #import a
-- #import b
-- (* #importashead Definition b *)
-- #import c
-- #import conjjj
-- (* #importashead Lemma conjjj *)
-- #import aa
--
-- >>> pe (contract False =<< (ExceptT $ pss "z" zv psx))
-- module z
-- <: y
-- .
-- #importashead Definition zp
-- #importrest
--
-- >>> pe (contract True =<< (ExceptT $ pss "z" zv psx))
-- module z
-- <: y
-- .
-- #import y
-- #import a
-- #import b
-- #import c
-- #import conjjj
-- #import zp
-- (* #importashead Definition zp *)
-- #import aa
contract :: Monad m => Bool -> R.ResolvedFile -> ExceptT String m String
contract full rf@(R.ResolvedFile (A.File _ srcs) _) = do
  db <- R.resolvedObjs rf
  unlines <$> contractSrc full [] db srcs

contractSrc :: Monad m => Bool -> [(String, Int)] -> [C.Obj] -> [A.Src] -> ExceptT String m [String]

contractSrc _    _    _  [] = return []

contractSrc full used db (A.Verbatim _ v:t) = (v:) <$> contractSrc full used db t

contractSrc full used db (A.Coq line o:t) =
  case takeUntilCompact (\o' -> C.name o' == C.name o) db of
    Nothing -> addLine line $ throwError $ "Coq object not found: '" ++ C.name o ++ "'" ++ usedMsg (C.name o) used
    Just (_, deps, rest) -> (filter (/= "") (ftImport full <$> (deps ++ [o])) ++) <$> contractSrc full (((,line) . C.name <$> (deps ++ [o])) ++ used) rest t

contractSrc full used db (A.Ref line (A.Import name _):t) = do
  when (null db) $ addLine line $ throwError "Importing from empty list."
  case takeUntilCompact (\o -> C.name o == name) db of
    Nothing -> addLine line $ throwError $ "Import not found: '" ++ name ++ "'" ++ usedMsg name used
    Just (o, deps, rest) -> (filter (/= "") (ftImport full <$> (deps ++ [o])) ++) <$> contractSrc full (((,line) . C.name <$> (deps ++ [o])) ++ used) rest t

contractSrc full used db (A.Ref line (A.ImportDef name _):t) = do
  when (null db) $ addLine line $ throwError "Importing from empty list."
  case takeUntilCompact (\o -> C.name o == name) db of
    Nothing -> addLine line $ throwError $ "Import not found: '" ++ name ++ "'" ++ usedMsg name used
    Just (o, deps, rest) -> (filter (/= "") (ftImport full <$> (deps ++ [o])) ++) <$> contractSrc full (((,line) . C.name <$> (deps ++ [o])) ++ used) rest t

contractSrc full used db (A.Ref line (A.ImportAsHead _ name _ _):t) = do
  when (null db) $ addLine line $ throwError "Importing from empty list."
  case takeUntilCompact (\o -> C.name o == name) db of
    Nothing -> addLine line $ throwError $ "Import as head not found: '" ++ name ++ "'" ++ usedMsg name used
    Just (o, deps, rest) -> (filter (/= "")
      ((ftImport full <$> deps) ++
      if full then ["#import " ++ C.name o]
      else         [""]) ++) <$> contractSrc full (((,line) . C.name <$> (deps ++ [o])) ++ used) rest t

contractSrc full used db (A.Ref line (A.ImportDefsUpto name _):t) = do
  when (null db) $ addLine line $ throwError "Importing from empty list."
  case takeUntilCompact (\o -> C.name o == name) db of
    Nothing -> addLine line $ throwError $ "Import definitions upto not found: '" ++ name ++ "'" ++ usedMsg name used
    Just (_, deps, rest) -> ((if full then filter (/= "") (ftImport full <$> deps) else ["#importdefsupto " ++ name]) ++) <$> contractSrc full (((,line) . C.name <$> deps) ++ used) rest t

contractSrc full used db (A.Ref line (A.ImportRestDefs _):t) =
  let (defs, rest) = partition C.isDefinition db in
  (if full then ((fullImport  <$> defs) ++)
   else         ("#importrestdefs":)) <$> contractSrc full (((,line) . C.name <$> defs) ++ used) rest t

contractSrc full used db (A.Ref line (A.ImportRest _):t) =
  (if full then ((fullImport <$> db) ++)
   else         ("#importrest":)) <$> contractSrc full (((,line) . C.name <$> db) ++ used) [] t

contractSrc full used db (A.Parent _ (_, orig):t) = (orig:) <$> contractSrc full used db t

usedMsg :: String -> [(String, Int)] -> String
usedMsg key db =
  case filter (\e -> fst e == key) db of
  [] -> ""
  [h] -> ", already imported at line " ++ show (snd h)
  (h:t) -> ", internal error: multiple used locactions: " ++ show (h:t)

ftImport :: Bool -> C.Obj -> String
ftImport full = if full then fullImport else terseImport

terseImport :: C.Obj -> String
terseImport (C.Definition _) = ""
terseImport (C.Parameter v) = 
  let o = C.Parameter v in
  "#importashead " ++ toImpl (C.kind o) ++ " " ++ C.name o

fullImport :: C.Obj -> String
fullImport o =
  "#import " ++ C.name o ++
  if C.isDefinition o
  then ""
  else "\n(* #importashead " ++ toImpl (C.kind o) ++ " " ++ C.name o ++ " *)"

toImpl :: String -> String
toImpl k = if k == "Parameter" then "Definition" else "Lemma"

data RenderedSrc = Straight [String] | Imported [String] deriving (Eq, Show)

newObjs :: String -> [String] -> [C.Obj] -> Maybe (C.Obj, [C.Obj])
newObjs name used db = fmap (filter (\o -> C.name o `notElem` used)) <$> R.withDependencies name db

-- |
-- >>> :m +Data.Functor.Identity
-- >>> let runit = runIdentity . Control.Monad.Except.runExceptT
-- >>> runit $ renderSrcs [] [A.Verbatim 1 "a"] []
-- Right [Straight ["a"]]
--
-- >>> runit $ renderSrcs [] [A.Coq 0 (C.Definition "x")] []
-- Right [Straight ["x"]]
--
-- >>> runit $ renderSrcs [] [A.Coq 0 (C.Definition "x")] []
-- Right [Straight ["x"]]
--
-- >>> runit $ renderSrcs [] [A.Parent 0 ("", "x")] []
-- Right [Straight ["x"]]
--
-- >>> runit $ (flip (renderSrcs [])) [C.Definition "kind a"] [A.Ref 0 (A.Import "a" "orig")]
-- Right [Imported ["(* resolved orig *)","kind a"]]
--
-- >>> runit $ (flip (renderSrcs [])) [C.Definition "kind a\nbody"] [A.Ref 0 (A.ImportAsHead (Just "kind") "a" "body" "orig")]
-- Right [Imported ["(* resolved orig *)","kind a"],Straight ["body"]]
--
-- >>> runit $ (flip (renderSrcs [])) [C.Definition "kind a"] [A.Ref 0 (A.ImportAsHead (Just "kind") "a" "" "orig")]
-- Right [Imported ["(* resolved orig *)","kind a"]]
--
-- >>> runit $ (flip (renderSrcs [])) [C.Definition "kind x",C.Definition "kind a"] [A.Ref 0 (A.ImportDefsUpto "a" "orig")]
-- Right [Imported ["(* resolved orig *)","kind x"]]
--
-- >>> runit $ (flip (renderSrcs [])) [C.Parameter "kind x",C.Definition "kind a"] [A.Ref 0 (A.ImportRestDefs "orig")]
-- Right [Imported ["(* resolved orig *)","kind a"]]
--
-- >>> runit $ (flip (renderSrcs [])) [C.Parameter "kind x",C.Definition "kind a"] [A.Ref 0 (A.ImportRest "orig")]
-- Right [Imported ["(* resolved orig *)","kind x","kind a"]]
renderSrcs :: Monad m => [String] -> [A.Src] -> [C.Obj] -> ExceptT String m [RenderedSrc]
renderSrcs _    []                   _ = return []
renderSrcs used (A.Verbatim _ c:t)  db = (Straight [c]:) <$> renderSrcs used t db
renderSrcs used (A.Coq _ o:t) db = (Straight (lines $ C.value o):) <$> renderSrcs (C.name o:used) t db
renderSrcs used (A.Parent _ (_, orig):t) db = (Straight [orig]:) <$> renderSrcs used t db
renderSrcs used (A.Ref line (A.Import name orig):t) db =
  case newObjs name used db of
  Nothing -> throwError $ show line ++ ": Name not found: " ++ name
  Just (o, deps) ->
    let full = deps ++ [o] in
    (Imported (("(* resolved " ++ orig ++ " *)"):((lines . C.value) =<< full)):) <$> renderSrcs ((C.name <$> full) ++ used) t db

renderSrcs used (A.Ref line (A.ImportDef name orig):t) db =
  case newObjs name used db of
  Nothing -> throwError $ show line ++ ": Name not found: " ++ name
  Just (o, deps) ->
    let full = deps ++ [o] in
    (Imported (("(* resolved " ++ orig ++ " *)"):((lines . C.value) =<< full)):) <$> renderSrcs ((C.name <$> full) ++ used) t db

renderSrcs used (A.Ref line (A.ImportAsHead kind name body orig):t) db = 
  case newObjs name used db of
  Nothing -> throwError $ show line ++ ": Name not found: " ++ name
  Just (o, deps) -> do
    when (isJust kind && Just True /= (all (uncurry (==)) . zip (C.value o) <$> kind))
      (throwError $ "Internal error: kind in ImportAsHead and Obj do not match: '" ++ C.value o ++ "' does not start with '" ++ fromMaybe undefined kind ++ "'")
    when (reverse body /= take (length body) (reverse $ C.value o))
      (throwError $ "Internal error: body in ImportAsHead and Obj do not match: '" ++ C.value o ++ "' does not end with '" ++ body ++ "'")
    let full = deps ++ [(if null body then id else C.mutate (dropEnd (length body + 1))) o]
    tres <- renderSrcs (C.name o:(C.name <$> deps) ++ used) t db
    return
      ((Imported (("(* resolved " ++ orig ++ " *)"):((lines . C.value) =<< full)):)
       . (if null body then id else (Straight (lines body):))
       $ tres)

renderSrcs used (A.Ref line (A.ImportDefsUpto name orig):t) db = 
  case newObjs name used db of
  Nothing -> throwError $ show line ++ ": Name not found: " ++ name
  Just (_, deps) ->
   (Imported (("(* resolved " ++ orig ++ " *)"):((lines . C.value) =<< deps)):) <$> renderSrcs ((C.name <$> deps) ++ used) t db

renderSrcs used (A.Ref _ (A.ImportRestDefs orig):t) db = 
  let defs = filter (\o -> C.name o `notElem` used) $ filter C.isDefinition db in
  (Imported (("(* resolved " ++ orig ++ " *)"):((lines . C.value) =<< defs)):) <$> renderSrcs ((C.name <$> defs) ++ used) t db

renderSrcs used (A.Ref _ (A.ImportRest orig):t) db = 
  let objs = filter (\o -> C.name o `notElem` used) db in
  (Imported (("(* resolved " ++ orig ++ " *)"):((lines . C.value) =<< objs)):) <$> renderSrcs ((C.name <$> objs) ++ used) t db
