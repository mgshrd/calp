{-# LANGUAGE TupleSections #-}
{-# LANGUAGE FlexibleContexts #-}

module Str2Ast (
  parse,
) where

{- HLINT ignore "Functor law" -}

import Control.Monad.Except (ExceptT(..), throwError, withExceptT, liftEither)
import Control.Monad.State (StateT, get, put, modify)
import Control.Monad.Morph (lift)

import qualified Data.List as L (isPrefixOf, isInfixOf)

import qualified Coq as C

import StdLib (orElse, runEST)

import Utils (ebyamMorf, cleanup)

import Ast as A

-- doctest setup.
-- $setup
-- >>> let line = 123 :: Int
-- >>> let runit' = \m -> Control.Monad.State.runStateT (Control.Monad.Except.runExceptT m) line
-- >>> let runix = \a m -> let _ = a :: Int in runit' m
-- >>> let runit = \offs m -> fmap (fmap (== line + offs)) $ runit' m

parse :: Monad m => String -> [String] -> ExceptT String m File
parse name content = do
  content' <- liftEither $ sequence (cleanup <$> content)
  withExceptT (\(msg, line) -> name ++ ":" ++ show line ++ ": " ++ msg) $ parseFile name content'

parseFile :: Monad m => String -> [String] -> ExceptT (String, Int) m File
parseFile name xs = fst <$> runEST 1 (parseState name xs)

-- |
-- >>> :{
-- runit 1 $ parseState "x" [
--                "a"
--           ]
-- :}
-- (Right (File "x" [Verbatim ... "a"]),True)
--
-- >>> :{
-- runit 1 $ parseState "x" [
--                  "a"
--                , "<: "
--           ]
-- :}
-- (Left "Bad parent line: '<: '",True)
--
-- >>> :{
-- runit 4 $ parseState "x" [
--                  "Definition a : nat."
--                , "Proof."
--                , "  exact 1."
--                , "Defined."
--           ]
-- :}
-- (Right (File "x" [Coq ... (Definition "Definition a : nat.\nProof.\n  exact 1.\nDefined.")]),True)
--
-- >>> :{
-- runit 7 $ parseState "x" [
--                  "a"
--                , "<: x"
--                , ""
--                , "Definition a : nat."
--                , "Proof."
--                , "  exact 1."
--                , "Defined."
--           ]
-- :}
-- (Right (File "x" [Verbatim ... "a",Parent ... ("x","<: x"),Verbatim ... "",Coq ... (Definition "Definition a : nat.\nProof.\n  exact 1.\nDefined.")]),True)
--
-- >>> :{
-- runit 6 $ parseState "x" [
--                  "a"
--                , "<: x"
--                , "Definition a : nat."
--                , "Proof."
--                , "  exact 1."
--                , "Defined."
--                , "#import "
--           ]
-- :}
-- (Left "Bad import line: '#import '",True)
--
-- >>> :{
-- runit 5 $ parseState "x" [
--                  "a"
--                , "<: x"
--                , ""
--                , "Definition a : nat."
--                , "Proof."
--                , "exact 1."
--                , "Defined."
--           ]
-- :}
-- (Left "Expected either a body closing verb, a line starting with ' ' (space), or an empty line: 'exact 1.'",True)
--
-- >>> :{
-- runit 7 $ parseState "x" [
--                  "a"
--                , "<: x"
--                , ""
--                , "Definition a : nat."
--                , "Proof."
--                , " exact 1."
--                , "Defined."
--                , "#import "
--           ]
-- :}
-- (Left "Bad import line: '#import '",True)
parseState :: Monad m => String -> [String] -> ExceptT String (StateT Int m) File
parseState name [] = return $ File name []
parseState name (h:t) = do
  line <- lift get

  obj' <- parseObj (h:t)
  postObjLine <- lift get
  let obj = (postObjLine,) <$> fmap (Coq line) <$> obj'

  lift $ put line
  ref' <- parseRef (h:t)
  postRefLine <- lift get
  let ref = (postRefLine,) <$> fmap (Ref line) <$> ref'

  lift $ put line
  par' <- parseParent h
  postParLine <- lift get
  let par = (postParLine,) <$> Parent line <$> par'

  let (resLine, (t', res)) =
                   obj            `orElse`
                   ref            `orElse`
                   (fmap (t,) <$> par) `ebyamMorf`
                   (line + 1, (t, Verbatim line h))
  put resLine
  tres <- parseState name t'
  return $ prependSrc res tres


-- |
-- >>> runit 0 $ parseParent "Definition a := 1."
-- (Right Nothing,True)
--
-- >>> runit 0 $ parseParent "<: "
-- (Left "Bad parent line: '<: '",True)
--
-- >>> runit 1 $ parseParent "<: x y z"
-- (Right (Just ("x","<: x y z")),True)
parseParent :: Monad m => String -> ExceptT String (StateT Int m) (Maybe (String, String))
parseParent l
  | "<: " `L.isPrefixOf` l =
    let tokens = tail $ words l in
    if null tokens
    then throwError $ "Bad parent line: '" ++ l ++ "'"
    else lift (modify (+1)) >> return (Just (head tokens, l))
  | otherwise              = return Nothing

-- |
-- >>> runit 0 $ parseRef ["Definition a := 1."]
-- (Right Nothing,True)
--
-- >>> runit 0 $ parseRef ["#import "]
-- (Left "Bad import line: '#import '",True)
--
-- >>> runit 1 $ parseRef ["#import a"]
-- (Right (Just ([],Import "a" "#import a")),True)
--
-- >>> runit 1 $ parseRef ["#importdef a"]
-- (Right (Just ([],ImportDef "a" "#importdef a")),True)
--
-- >>> runit 1 $ parseRef ["#importashead a"]
-- (Left "Body is missing.",True)
--
-- >>> runit 4 $ parseRef ["#importashead a","Proof.","","Qed."]
-- (Right (Just ([],ImportAsHead Nothing "a" "Proof.\n\nQed." "#importashead a")),True)
--
-- >>> runit 4 $ parseRef ["#importashead kind  a","Proof.","","Qed.","rest"]
-- (Right (Just (["rest"],ImportAsHead (Just "kind") "a" "Proof.\n\nQed." "#importashead kind  a")),True)
--
-- >>> runit 0 $ parseRef ["#importdefsupto a x"]
-- (Left "Bad import line: '#importdefsupto a x'",True)
--
-- >>> runit 1 $ parseRef ["#importdefsupto a"]
-- (Right (Just ([],ImportDefsUpto "a" "#importdefsupto a")),True)
parseRef :: Monad m => [String] -> ExceptT String (StateT Int m) (Maybe ([String], RefKind))
parseRef [] = throwError "Internal error: cannot parse ref from empty lines"
parseRef (l:t)
  | "#import "         `L.isPrefixOf` l =
    let tokens = tail $ words l in
      if length tokens == 1
      then lift (modify (+1)) >> return (Just (t, Import (head tokens) l))
      else throwError $ "Bad import line: '" ++ l ++ "'"
  | "#importdef "      `L.isPrefixOf` l =
    let tokens = tail $ words l in
      if length tokens == 1
      then lift (modify (+1)) >> return (Just (t, ImportDef (head tokens) l))
      else throwError $ "Bad import line: '" ++ l ++ "'"
  | "#importashead "   `L.isPrefixOf` l =
    let tokens = tail $ words l in
    let kind = if length tokens == 2 then head tokens else "" in
      if (length tokens == 1) || (length tokens == 2)
      then do
        lift $ modify (+1)
        (t', body) <- if kind == "Axiom" then return (t, "") else parseBody t
        return $ Just (
            t',
            ImportAsHead
              (if null (tail tokens)
               then Nothing
               else Just $ head tokens)
              (last tokens)
              body
              l)
      else throwError $ "Bad import line: '" ++ l ++ "'"
  | "#importdefsupto " `L.isPrefixOf` l =
      let tokens = tail $ words l in
      if length tokens == 1
      then lift (modify (+1)) >> return (Just (t, ImportDefsUpto (head tokens) l))
      else throwError $ "Bad import line: '" ++ l ++ "'"
  | "#importrestdefs"  `L.isPrefixOf` l =
      let tokens = tail $ words l in
      if null tokens
      then lift (modify (+1)) >> return (Just (t, ImportRestDefs l))
      else throwError $ "Bad import line: '" ++ l ++ "'"
  | "#importrest"      `L.isPrefixOf` l =
      let tokens = tail $ words l in
      if null tokens
      then lift (modify (+1)) >> return (Just (t, ImportRest l))
      else throwError $ "Bad import line: '" ++ l ++ "'"
  | otherwise                           = return Nothing

prependSrc :: Src -> File -> File
prependSrc src (File name ss) = File name (src:ss)

-- |
-- >>> runit 0 $ parseObj ["asdf Conjecture conj :", " ."]
-- (Right Nothing,True)
--
-- >>> runit 2 $ parseObj ["Conjecture conj :", " ."]
-- (Right (Just ([],Parameter "Conjecture conj :\n .")),True)
--
-- >>> runit 4 $ parseObj ["Corollary cor :", " .", "Proof.", "Qed."]
-- (Right (Just ([],Definition "Corollary cor :\n .\nProof.\nQed.")),True)
--
-- >>> runit 3 $ parseObj ["Corollary cor : .", "Proof.", "Qed."]
-- (Right (Just ([],Definition "Corollary cor : .\nProof.\nQed.")),True)
--
-- >>> runit 2 $ parseObj ["Conjecture conj :", " .", "Corollary cor :", " .", "Proof.", "Qed."]
-- (Right (Just ([...],Parameter "Conjecture conj :\n .")),True)
--
-- >>> runit 4 $ parseObj ["Definition a : nat.", "Proof.", "  exact 1.", "Defined.","rest"]
-- (Right (Just ([...],Definition "Definition a : nat.\nProof.\n  exact 1.\nDefined.")),True)
parseObj :: Monad m => [String] -> ExceptT String (StateT Int m) (Maybe ([String], C.Obj))
parseObj [] = return Nothing
parseObj (h:t)
  | "Conjecture"  `L.isPrefixOf` h = wrap $ parseAssumption h t
  | "Hypothesis"  `L.isPrefixOf` h = wrap $ parseAssumption h t
  | "Parameter"   `L.isPrefixOf` h = wrap $ parseAssumption h t
  | "Variable"    `L.isPrefixOf` h = wrap $ parseAssumption h t
  | "Axiom"       `L.isPrefixOf` h = wrap $ parseAssertion h t -- Assertion instead of assumption because marking the assumption as an axiom is treated as a signal of not wanting to provide a definition
  | "Corollary"   `L.isPrefixOf` h = wrap $ parseAssertion h t
  | "Definition"  `L.isPrefixOf` h = wrap $ parseAssertion h t
  | "Example"     `L.isPrefixOf` h = wrap $ parseAssertion h t
  | "Fact"        `L.isPrefixOf` h = wrap $ parseAssertion h t
  | "Lemma"       `L.isPrefixOf` h = wrap $ parseAssertion h t
  | "Proposition" `L.isPrefixOf` h = wrap $ parseAssertion h t
  | "Remark"      `L.isPrefixOf` h = wrap $ parseAssertion h t
  | "Theorem"     `L.isPrefixOf` h = wrap $ parseAssertion h t
  | otherwise                     = return Nothing
  where wrap m = Just <$> m

parseAssumption :: Monad m => String -> [String] -> ExceptT String (StateT Int m) ([String], C.Obj)
parseAssumption h t = do
  (rest, hd) <- parseHead h t
  return (rest, C.Parameter hd)

parseAssertion:: Monad m => String -> [String] -> ExceptT String (StateT Int m) ([String], C.Obj)
parseAssertion h t = do
  (rest, (hd, body)) <- parseWithBody h t
  return (rest, C.Definition (hd ++ if null body then "" else "\n" ++ body))

--
-- >>> runit 2 $ parseWithBody "x" ["Definition a := 1.", "rest"]
-- (Right (["rest"],"Definition a := 1."),True)
parseWithBody :: Monad m => String -> [String] -> ExceptT String (StateT Int m) ([String] {-rest'-}, (String{-head-}, String{-body-}))
parseWithBody intro rest = do
    (rest', hd) <- parseHead intro rest
    (rest'', body) <-
                if ("Axiom " `L.isInfixOf` hd) || (":=" `L.isInfixOf` hd)
                then return (rest', "")
                else parseBody rest'
    return (rest'', (hd, body))

-- Look for the first line ending with '.'
-- Don't allow non-space starters in intermediate lines.
-- |
-- >>> runit 0 $ parseHead "x" []
-- (Left "Unexpected EOT while parsing head.",True)
--
-- >>> runit 1 $ parseHead "x." ["rest"]
-- (Right (["rest"],"x."),True)
--
-- >>> runit 2 $ parseHead "x" [" h.", "rest"]
-- (Right (["rest"],"x\n h."),True)
parseHead :: Monad m => String -> [String] -> ExceptT String (StateT Int m) ([String], String)
parseHead intro []
  | last intro == '.' = lift (modify (+1)) >> return ([], intro)
  | otherwise         = throwError "Unexpected EOT while parsing head."
parseHead intro (h:t)
  | last intro == '.'          = lift (modify (+1)) >> return (h:t, intro)
  | not $ " " `L.isPrefixOf` h = throwError ("Head intermediary does not start with space: " ++ h)
  | last h == '.'              = lift (modify (+2)) >> return (t, intro ++ "\n" ++ h)
  | otherwise                  = lift (modify (+1)) >> parseHead (intro ++ "\n" ++ h) t

-- Look for the closing line ending with '.'
-- Don't allow non-space starters in intermediate lines.
-- |
-- >>> runit 2 $ parseBody ["Proof.", "Qed.", "rest"]
-- (Right (["rest"],"Proof.\nQed."),True)
--
-- >>> runit 1 $ parseBody ["Proof.","asfd","Qed."]
-- (Left "Expected either a body closing verb, a line starting with ' ' (space), or an empty line: 'asfd'",True)
{- HLINT ignore "Use ||" -}
parseBody :: Monad m => [String] -> ExceptT String (StateT Int m) ([String], String)
parseBody [] = throwError "Body is missing."
parseBody (h:t)
  | h == "Proof." = lift (modify (+1)) >> parseBodyLoop "Proof." t
  | otherwise     = throwError ("Body does not start with 'Proof.': \"" ++ h ++ "\"")

parseBodyLoop :: Monad m => String -> [String] -> ExceptT String (StateT Int m) ([String], String)
parseBodyLoop accu input =
       case input of
        [] -> throwError "Unexpected EOT while parsing body. Expected \"Defined.\" or \"Qed.\""
        h':t'
          | (" " `L.isPrefixOf` h') || (h' == "") -> lift (modify (+1)) >> parseBodyLoop (accu ++ "\n" ++ h') t'
          | or [h' == "Defined.", h' == "Qed."] -> lift (modify (+1)) >> return (t', accu ++ "\n" ++ h')
          | otherwise                           -> throwError $ "Expected either a body closing verb, a line starting with ' ' (space), or an empty line: '" ++ h' ++ "'"
