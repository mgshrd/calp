{-# LANGUAGE TupleSections #-}

module Resolver (
  ResolvedFile(..),
  resolvedObjs,
  resolveFile,
  withDependencies,
) where

import Data.List (find)
import Data.Maybe (isJust)

import Control.Arrow (left)
import Control.Monad (when)
import Control.Monad.Except (ExceptT, throwError, liftEither)

import qualified Coq as C

import Ast (File(..), Src(..), RefKind(..), parentNames)

import StdLib (takeUntilCompact)

import Utils (addFile, addLine, MergeRes(..), reduceM, mergeDAG)

data ResolvedFile = ResolvedFile File{-file-} [ResolvedFile]{-parents-} deriving (Eq, Show)

-- Nothing if key is not in dag.
-- |
-- >>> withDependencies "a" []
-- Nothing
--
-- >>> withDependencies "a" [C.Definition "d x"]
-- Nothing
--
-- >>> withDependencies "a" [C.Definition "k a"]
-- Just (Definition "k a",[])
--
-- >>> withDependencies "a" [C.Parameter "k p",C.Definition "k a 1",C.Definition "k a 2"]
-- Just (Definition "k a 1",[Parameter "k p"])
withDependencies :: String -> [C.Obj] {-dag-} -> Maybe (C.Obj, [C.Obj])
withDependencies key dag =
  let dec o = C.name o == key in
  (\res -> let (o, pre, _) = res in (o, pre)) <$> takeUntilCompact dec dag

-- Re-create the dag according to the definitons and import rules in the source lines.
--
-- |
-- >>> runExceptT $ mergeFile "" [] [] []
-- Right []
--
-- >>> runExceptT $ mergeFile "file" [Ref 0 (Import "a" "")] [] [C.Definition "kind dep val.", C.Definition "kind a val.", C.Definition "kind nondep"]
-- Right [Definition "kind dep val (* file:0 *).",Definition "kind a val (* file:0 *)."]
--
-- >>> runExceptT $ mergeFile "file" [Ref 0 (Import "a" "")] [] [C.Parameter "kind param.",C.Definition "kind dep val.", C.Definition "kind a val.", C.Definition "kind nondep"]
-- Left "0:error: Refusing to auto import ..."
--
-- >>> runExceptT $ mergeFile "file" [Ref 0 (Import "a" "")] [("param",1)] [C.Parameter "kind param.",C.Definition "kind dep val.", C.Definition "kind a val.", C.Definition "kind nondep"]
-- Right [Definition "kind dep val (* file:0 *).",Definition "kind a val (* file:0 *)."]
--
-- >>> runExceptT $ mergeFile "file" [Coq 0 (C.Definition "kind nondep")] [] [C.Parameter "kind param.",C.Definition "kind dep val.", C.Definition "kind a val.", C.Definition "kind nondep"]
-- Left "0:error: Redefinition...
mergeFile :: Monad m => String -> [Src] -> [(String, Int)] {-used-} -> [C.Obj] {-dag-} -> ExceptT String m [C.Obj]

mergeFile _    []             _    _   = return []

mergeFile file (Coq line o:t) used dag = do
  -- Coq objects don't automatically pull in depenendencies, use #importdefsupto (if the preferred #import or #importashead are not feasible for some reason)
  when (any (\o' -> C.isDefinition o' == C.isDefinition o && C.name o == C.name o') dag) $ addLine line $ throwError $ "Redefinition of '" ++ C.name o ++ "', use '" ++ (if C.isDefinition o then "#importdef " else "#import ") ++ C.name o ++ "' instead."
  res <- mergeFile file t ((C.name o, line):used) dag
  return $ C.annotate (file ++ ":" ++ show line) o:res

mergeFile file (Ref line (Import name _):t) used dag =
    mergeDelimited
      return
      file line name used dag True t

mergeFile file (Ref line (ImportDef name _):t) used dag =
    mergeDelimited
      (\o -> if C.isDefinition o then return o else addLine line $ throwError $ "Not a definition: " ++ name)
      file line name used dag True t

mergeFile file (Ref line (ImportAsHead kind name body _):t) used dag =
    mergeDelimited
      (liftEither . C.fill kind body)
      file line name used dag True t

mergeFile file (Ref line (ImportDefsUpto name _):t) used dag =
    mergeDelimited
      return
      file line name used dag False t

mergeFile file (Ref line (ImportRestDefs _):t) used dag =
  let defs = filter C.isDefinition dag in
  let new = C.annotate (file ++ ":" ++ show line) <$> filter (\o -> C.name o `notElem` (fst <$> used)) defs in
  let newUsed = ((\o -> (C.name o, line)) <$> new) ++ used in
  addLine line $ (new ++) <$> mergeFile file t newUsed dag

mergeFile file (Ref line (ImportRest _):t) used dag =
  let new = C.annotate (file ++ ":" ++ show line) <$> filter (\o -> C.name o `notElem` (fst <$> used)) dag in
  let newUsed = ((\o -> (C.name o, line)) <$> new) ++ used in
  addLine line $ (new ++) <$> mergeFile file t newUsed dag

mergeFile file (_:t) used dag = mergeFile file t used dag



mergeDelimited :: Monad m => (C.Obj -> ExceptT String m C.Obj) -> String -> Int -> String -> [(String, Int)] -> [C.Obj] -> Bool -> [Src] -> ExceptT String m [C.Obj]
mergeDelimited transf file line name used dag addImp t = do
  when (name `elem` (fst <$> used)) $ addLine line $ throwError $ "Importing an already existing object: " ++ name ++ " (Sprung into existence at line: " ++ show (maybe 0 snd $ find (\e -> fst e == name) used) ++ ")"
  (imp, deps) <- case withDependencies name (C.annotate (file ++ ":" ++ show line) <$> dag) of
    Nothing -> addLine line $ throwError $ "Import not found: " ++ name
    Just (imp', deps') -> (, deps') <$> transf imp'
  let newDeps = filter (\o -> C.name o `notElem` (fst <$> used)) deps
  when (any (not . C.isDefinition) newDeps) $ addLine line $ throwError $
   "Refusing to auto import assumption(s): " ++ show (("#import " ++) . C.name <$> filter (not . C.isDefinition) newDeps) ++ " triggered by " ++ name ++ " (#import them explicitly)"
  let newUsed = (name, line):(((\o -> (C.name o, line)) <$> newDeps) ++ used)
  (\dag' -> newDeps ++ (if addImp then imp:dag' else dag')) <$> mergeFile file t newUsed dag

-- Ordered object names. Used as implied dependencies between names.
-- Left: conflicting names
resolvedObjs :: Monad m => ResolvedFile -> ExceptT String m [C.Obj]
resolvedObjs (ResolvedFile (File name srcs) parents) = addFile name $ do
  parentObjs       <- sequence $ resolvedObjs <$> parents
  mergedParentObjs <- liftEither $ left show (reduceM (Right []) (mergeDAG mergeObjs) (parentObjs :: [[C.Obj]]) :: Either (C.Obj, C.Obj) [C.Obj])
  mergeFile name srcs [] (mergedParentObjs :: [C.Obj])

{- HLINT ignore "Use guards" -}
mergeObjs :: C.Obj -> [C.Obj] -> C.Obj -> [C.Obj] -> MergeRes
mergeObjs h1 t1 h2 t2 =
  if C.name h1 == C.name h2
  then if C.isDefinition h1 == C.isDefinition h2
       then Merge
       else if C.isDefinition h1
            then TakeFirst
            else TakeSecond
  else if isJust $ find (\o -> C.name o == C.name h1) t2 -- n1 depends on n2
       then if isJust $ find (\o -> C.name o == C.name h2) t1 -- n2 depends on n1
            then Conflict
            else TakeSecond
       else TakeFirst

-- $setup
-- >>> :m +Data.List
-- >>> :m +Str2Ast
-- >>> :m +StdLib
-- >>> :m +Control.Monad.Morph
-- >>> :m +Control.Monad.Except
-- >>> let loader file ff = do { path' <- lift $ (ff :: String -> IO (Either String FilePath)) $ file ++ ".v"; path <- liftEither path'; lns <- lift $ readLines path; parse file lns }
-- >>> let pss name content ff = runExceptT $ do { f <- parse name content; rf <- Resolver.resolveFile ff f; return rf }
-- >>> :{
-- let (xv, yv, zv, findPar) = (\(x, y, z) -> (x, y, z, \k -> case Data.List.lookup k [("x", xv), ("y", yv), ("z", zv)] of { Nothing -> Left "not found"; Just e -> Right e })
--                                 ) ( 
--                                     [
--                                       "Definition a := 1.",
--                                       "Parameter b : nat.",
--                                       "Conjecture c : Prop."
--                                     ],
--                                     [
--                                       "<: x",
--                                       ".",
--                                       "Definition y := 1.",
--                                       "#importashead c",
--                                       "Proof.",
--                                       "  c proof",
--                                       "Qed.",
--                                       "#importrest"
--                                     ],
--                                     [
--                                       "<: y",
--                                       ".",
--                                       "#importashead b",
--                                       "Proof.",
--                                       "Defined.",
--                                       "#importrest"
--                                     ]
--                                   )
-- :}

-- |
-- >>> runExceptT $ resolveFile (\_ -> liftEither $ Left "") =<< (Str2Ast.parse "x" xv)
-- Right (ResolvedFile (File "x" [...]) [])
--
-- >>> runExceptT $ resolveFile (\n -> Str2Ast.parse n =<< liftEither (findPar n)) =<< (Str2Ast.parse "y" yv)
-- Right (ResolvedFile (File "y" [...]) [ResolvedFile (File "x" [...]) []])
--
-- >>> runExceptT $ resolveFile (\n -> Str2Ast.parse n =<< liftEither (findPar n)) =<< (Str2Ast.parse "z" zv)
-- Right (ResolvedFile (File "z" [...]) [ResolvedFile (File "y" [...]) [...]])
resolveFile :: Monad m => (String -> ExceptT String m File) -> File -> ExceptT String m ResolvedFile
resolveFile resolver f = do
  parents <- sequence $ resolver <$> parentNames f
  resolvedParents <- sequence $ resolveFile resolver <$> parents
  return $ ResolvedFile f resolvedParents
