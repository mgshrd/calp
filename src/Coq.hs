module Coq (
  Obj(..),
  annotate,
  fill,
  isDefinition,
  kind,
  mutate,
  name,
  value,
) where

import qualified Data.List as L (dropWhile)
import qualified Data.Maybe as M (fromMaybe)

import StdLib (splitWhere)

data Obj
  = Definition String
  | Parameter String
    deriving (Eq, Ord, Show)

value :: Obj -> String
value (Definition v) = v
value (Parameter v) = v

kind :: Obj -> String
kind = head . words . value

-- |
-- >>> name (Definition "a b c\nd e f")
-- "b"
name :: Obj -> String
name = (head . tail) . words . value

mutate :: (String -> String) -> Obj -> Obj
mutate f (Definition s) = Definition (f s)
mutate f (Parameter s)  = Parameter (f s)

-- |
-- >>> annotate "x" (Definition "a b c\nd e f")
-- Definition "a b c (* x *)\nd e f"
--
-- >>> annotate "x" (Definition "Definition x := 1.")
-- Definition "Definition x := 1 (* x *)."
--
-- >>> annotate "x" (Definition "a b c.\nd e f")
-- Definition "a b c (* x *).\nd e f"
annotate :: String -> Obj -> Obj
annotate tag =
  let splitPred c = (c /= '\n') && (c /= '.') in
  let intro = takeWhile splitPred in
  let outro = L.dropWhile splitPred in
  mutate (\v -> intro v ++ " (* " ++ tag ++ " *)" ++ outro v)

isDefinition :: Obj -> Bool
isDefinition (Definition _) = True
isDefinition (Parameter _)  = False

-- |
-- >>> fill Nothing "b" (Definition "d")
-- Left ...
--
-- >>> fill (Just "a") "b" (Definition "d")
-- Left ...
--
-- >>> fill (Just "newkind") "body" (Parameter "kind name")
-- Right (Definition "newkind name\nbody")
fill :: Maybe String -> String -> Obj -> Either String Obj
fill _               _    o@(Definition _) = Left $ "'" ++ name o ++ "' already defined."
fill (Just "Axiom")  ""     (Parameter s)  = Definition <$> fillKind (Just "Axiom") s
fill _               ""   o@(Parameter _)  = Left $ "Cannot define '" ++ name o ++ "' with an empty body."
fill kind'           body   (Parameter s)  = (\ns -> Definition $ ns ++ "\n" ++ body) <$> fillKind kind' s

-- |
-- >>> fillKind Nothing "a\nb"
-- Left "Missing kind."
--
-- >>> fillKind Nothing "a \nb"
-- Left "Unknown kind: a"
--
-- >>> fillKind Nothing "Parameter \nb"
-- Right "Definition \nb"
--
-- >>> fillKind Nothing "Conjecture \nb"
-- Right "Lemma \nb"
--
-- >>> fillKind (Just "k") "a \nb"
-- Right "k \nb"
fillKind :: Maybe String -> String -> Either String String
fillKind k s =
  let (origKind, rest) = splitWhere (' ' ==) s in
  (\k' -> M.fromMaybe k' k ++ rest) <$> mapKind k origKind
  where
    mapKind _        ""           = Left "Missing kind."
    mapKind (Just k') _           = Right k'
    mapKind _        "Parameter"  = Right "Definition"
    mapKind _        "Conjecture" = Right "Lemma"
    mapKind Nothing  k'           = Left $ "Unknown kind: " ++ k'
