DUMPHTML = links -width 500 -dump

RUNHASKELL ?= runhaskell

GHC ?= ghc
GHCFLAGS ?= -Wall

DOCTEST ?= doctest
DOCTESTFLAGS ?= $(GHCFLAGS)

HLINT ?= hlint
HLINTFLAGS ?=

MARKDOWN ?= markdown
MARKDOWNFLAGS ?=

GIT ?= git
MAJOR = 0
MINOR = 0
COMMITCOUNT = $(shell $(GIT) rev-list --count HEAD)
PATCH = $(shell expr $(COMMITCOUNT) - 1)
EXTRA ?= $(shell git symbolic-ref --short HEAD | sed -e 's/^master$$//' -e 's/^./-&/')
DIRTY ?= $(shell git status --short | tr -d \\n | sed -e 's/..*/_dirty-'`date +'%Y-%m-%d-%H_%M_%S'`'/')
VER = $(MAJOR).$(MINOR).$(PATCH)$(EXTRA)$(DIRTY)

NAME = calp

SOURCES = Main Str2Ast Ast Coq Resolver Ast2Str StdLib Utils

all: lint test $(NAME)$(EXTRA)

README.html: README.md
	$(MARKDOWN) $(MARKDOWNFLAGS) $< > $@

$(NAME)$(EXTRA): $(SOURCES:%=src/%.hs)
	$(GHC) -isrc $(GHCFLAGS) $< -o $@

test: $(SOURCES:%=src/%.hs)
	$(DOCTEST) -isrc $(DOCTESTFLAGS) $<

ver:
	echo $(VER)

lint:
	$(HLINT) $(HLINTFLAGS) src

clean:
	rm -f src/*.o src/*.hi $(NAME)$(EXTRA)

dist: src-dist bin-dist

bin-dist: $(NAME)$(EXTRA)
	tar czf $(NAME)-hs-bin-$(VER)-$(shell uname -s)-$(shell uname -m).tar.gz $^

src-dist: $(shell ls src/*.hs)
	tar -C src -czf $(NAME)-hs-src-$(VER).tar.gz $(notdir $^)
