*Coq à la presse* (`calp`) is a tool to help Coq authors using the module system, by automatically
copying the mandatory definitions from the extended modules into the extending
module.

`calp` does not parse Coq syntax, so in order for it to recognize the structure
of your code, you have to format your source according to the line oriented
[conventions][conventions] detailed below. `calp` also assumes, that all
declarations depend on all the definitions before it, and uses this assumption
when auto importing definitions.

Syntax
------

Recognized preprocessor commands:

`#import name`: imports the entity from the parent as is, and all of its
dependencies.

`#importdef name`: imports the entity from the parent as is, and all of its
dependencies. Fails if `name` or any of its dependencies are not definitions.
This is to avoid accidentally adding assumptions.

`#importashead [new type] name`: imports the entity from the parent, and
replaces its type with either the `new type` (if specified), or `Definition` or
`Lemma`, depending on the original declaration. If the new type is _not_
`Axiom` a `Proof.` body is expected to follow.

`#importdefsupto name`: imports the entities from the parent, up to, but not
including `name`. Useful if `#importashead` does not work for some reason.

`#importrestdefs`: import the as of yet not imported definitions. Useful to put
it at the end of module definitions, so that the parents can freely add new
definitions.

`#importrest`: import _all_ not imported entities, including `Parameter`s and
other assumptions. Useful for intermediary `Module Type`s who wish to forward
their upstream parameters.

After preprocessing, the original preprocessor commands are left in the file,
along with their expansion, for the `-unprocess` inversion to work, you should
leave those lines as-is.

Command Line
------------

The command line parsing requires is a little wacky, so you should follow the
examples below closely. (See StdLib.hs:namedArgs for details).

`calp <file>`: Print the processed file contents to the standard output.

`calp -unprocess "" <file>`: Print the original file to the standard output.
Only works if the original preprocessing lines are left untouched.

`calp -in-place "" <file>`: Replace file contents with the preprocessed value.
Keeps the modification time.

`calp -unprocess -in-place "" <file>`: Replace file contents with the original
value. Keeps the modification time.

`calp -dump "" <file>`: Print a skeleton of `file` to be used as a base for
extensions.

[conventions]: Source Conventions
------------------

	Module M
	<: Parent1 (1)
	<: Parent2 (1)
	.

	Definition def1 := 1.

	Definition def2
 	          (x : nat)
	  : nat.
	Proof. (3)
	  exact 2.
	Defined.

	Parameter param : nat.

- one module (type) per file
- each extension declaration must be on its own line [1]
- the definitions within the module body must start at the beginning of the line
- only the intro vernacular command (`Definition`, `Lemma`, etc), `Proof.`, and `Defined.` or `Qed.` may be at the beginning of the line while defining entities
- `Proof.` is mandatory. [3]

Example
-------

### File `M.v`

	Module Type M.

	Definition d1 := 10.
	Parameter m : bool.

	Definition d2a := 20.
	Definition d2b := 21.
	Conjecture p : d2a = d1+d1.

	Definition d3 := 30.
	Conjecture q : 1 = 2.

	Definition d4 := 40.

	End M.

### File `N.v`

	Require Import M.

	Module N
	<: M
	.

	(* d1 is implicitly imported. *)
	#importashead m
	Proof.
	  exact true.
	Defined.

	(* d2a&d2b are implicitly imported. *)
	#importashead p
	Proof.
	  unfold d1, d2a. compute. reflexivity.
	Qed.

	(* d2 is implicitly imported *)
	#importashead Axiom q
	(* No body required. *)

	#importrestdefs

	(* If a new Parameter is added in M.v, coqc will fail. *)
	End N.
